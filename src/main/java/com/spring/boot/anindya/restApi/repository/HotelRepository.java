package com.spring.boot.anindya.restApi.repository;

import com.spring.boot.anindya.restApi.model.Hotel;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface HotelRepository extends MongoRepository<Hotel,String> {
    List<Hotel> findByName(String name);
   /* @Query(sort="{pricePerNight : -1}")
    List<Hotel> sortByPricePerNight();*/
}
