package com.spring.boot.anindya.restApi.com.controller;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("classController/")
public class MainController {
    @RequestMapping(value="getMethod",produces={"application/json"})
    public String getMethod(){
            return "Hello World";
    }
}
