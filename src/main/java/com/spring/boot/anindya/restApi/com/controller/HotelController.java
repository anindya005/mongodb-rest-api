package com.spring.boot.anindya.restApi.com.controller;

import com.spring.boot.anindya.restApi.model.Hotel;
import com.spring.boot.anindya.restApi.repository.HotelRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Comparator;
import java.util.List;
@RequestMapping(value="Hotel")
@RestController
public class HotelController {
    @Autowired
    HotelRepository hotelRepository;
    @GetMapping("/getHotels")
    public List<Hotel> getAllHotels(){
        return hotelRepository.findAll();
    }

    @GetMapping("/getHotels/{name}")
    public List<Hotel> getAllHotels(@PathVariable String name){
        return hotelRepository.findByName(name);
    }
    @GetMapping("/sortByPrice")
    public List<Hotel> sortByPricePerNight(){
        List<Hotel> sortedHotels = hotelRepository.findAll();
        sortedHotels.sort(new Comparator<Hotel>() {
            @Override
            public int compare(Hotel o1, Hotel o2) {
               if (o1.getPricePerNight()>o2.getPricePerNight()) return 1;
               else return -1;
            }
        });
        return sortedHotels;
    }
}
