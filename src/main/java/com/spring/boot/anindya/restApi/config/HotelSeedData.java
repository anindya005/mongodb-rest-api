package com.spring.boot.anindya.restApi.config;

import com.spring.boot.anindya.restApi.model.Address;
import com.spring.boot.anindya.restApi.model.Hotel;
import com.spring.boot.anindya.restApi.model.Review;
import com.spring.boot.anindya.restApi.repository.HotelRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.util.Arrays;
@Component
public class HotelSeedData implements CommandLineRunner {
    @Autowired
    HotelRepository hotelRepository;
    @Override
    public void run(String... args) throws Exception {
        Hotel taj = new Hotel(
                "Taj Hotel",
                9000,
                new Address("Mumbai","India"),
                Arrays.asList(
                        new Review("Amal",9,true))
        );
        Hotel casadona = new Hotel(
                "Casadona",
                13000,
                new Address("Dubai","Saudi Arab"),
                Arrays.asList(
                        new Review("Sekh",9,true),
                        new Review("Abdul",7,false))
        );
        Hotel pride = new Hotel(
                "Pride",
                7000,
                new Address("Paris","France"),
                Arrays.asList()
        );
        hotelRepository.deleteAll();
        hotelRepository.saveAll(Arrays.asList(taj,casadona,pride));
    }
}
