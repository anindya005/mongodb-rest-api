package com.spring.boot.anindya.restApi.repository;

import com.spring.boot.anindya.restApi.model.Book;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BookRepository extends MongoRepository<Book,Integer> {
}
